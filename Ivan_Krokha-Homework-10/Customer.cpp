#include "Customer.h"

Customer::Customer(std::string name) : _name(name)
{
}

Customer::Customer()
{
}

Customer::~Customer()
{
}

double Customer::totalSum() const
{
	double sum = 0;
	for (Item item : this->_items) {
		sum += item.totalPrice();
	}
	return sum;
}

void Customer::addItem(Item& item)
{
	if(!changeIfFound(item, 1)) this->_items.insert(item);
}

void Customer::removeItem(Item& item)
{
	if (!(changeIfFound(item, -1)))
		std::cout << "[!] Item not found" << std::endl;
}

void Customer::printItems() const
{
	std::cout << "Customer " << this->_name << " :" << std::endl;
	int cnt = 1;
	for (Item item : this->_items) {
		std::cout << cnt << ". " << item << std::endl;
	}
}

bool Customer::changeIfFound(Item toFind, int delta)
{
	auto pos = this->_items.find(toFind);
	if (pos != this->_items.end()) {
		Item temp(*pos);
		try
		{
			temp += delta;
		}
		catch (const BadCountException&)
		{
			this->_items.erase(pos);
			return true;
		}
		
		this->_items.erase(pos);
		this->_items.insert(temp);
		return true;
	}
	else return false;
}

