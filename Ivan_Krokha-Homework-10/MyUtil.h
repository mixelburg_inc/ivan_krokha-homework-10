#pragma once
#include <string>
#include <sstream>
#include <iostream>
#include <map>


namespace MyUtil
{

	enum class Msgs {
		enter_choice,
		enter_name,

		invalid_choice_error,
		input_type_error,
	};

 
	static std::map<Msgs, std::string> MSG = {
		// info messages
		{Msgs::enter_choice, "enter choice: "},
		{Msgs::enter_name, "enter name: "},

		// error messages
		{Msgs::invalid_choice_error, "[!] invalid choice"},
		{Msgs::input_type_error, "[!] only numbers allowed, try again"},
	};

	
	/**
	* @brief checks if string is integer number
	* @param s string
	* @return bool
	*/
	static bool isNum(const std::string& s)
	{
		// get iterator
		std::string::const_iterator it = s.begin();
		// go trough string
		while (it != s.end() && std::isdigit(*it)) ++it;

		// check if there is any non digit char
		return !s.empty() && it == s.end();
	}

	
	/**
	 * @brief converts string to int
	 * @param str string
	 * @return int from string
	*/
	static int toInt(const std::string& str)
	{
		// get string stream
		std::stringstream sstream(str);
		// create variable for the result
		int result;
		// convert string to int
		sstream >> result;

		return result;
	}

	
	/**
	 * @brief gets string from user
	 * @param message - message to be printed to user
	 * @return string input from user
	*/
	static std::string getString(std::string& message)
	{
		// create variable for input
		std::string str;
		// display given message
		std::cout << message;
		// get string from user
		std::getline(std::cin, str);

		return str;
	}


	/**
	 * @brief gets integer number from user
	 * @param message - message to be printed to user
	 * @param min_value - minimal valid value
	 * @param max_value - maximal valid value
	 * @return integer number from user
	*/
	static int getNum(std::string& message, int min_value = INT_MIN, int max_value = INT_MAX)
	{
		// variable for user input
		std::string input = MyUtil::getString(message);

		// get new input until it isn't correct
		while (!(MyUtil::isNum(input)))
		{
			// print error message
			std::cout << MyUtil::MSG[Msgs::input_type_error] << std::endl;

			// get new user input
			input = MyUtil::getString(message);
		}

		// get new input untill it's valid
		if (MyUtil::toInt(input) < min_value or MyUtil::toInt(input) > max_value)
		{
			std::cout << "only numbers between " << min_value << " and " << max_value << " are allowed" << std::endl;
			return MyUtil::getNum(message, min_value, max_value);
		}
		return MyUtil::toInt(input);
	}
}
