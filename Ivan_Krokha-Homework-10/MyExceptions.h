#pragma once
#include <exception>

struct BadPriceException : public std::exception {
    const char* what() const throw () {
        return "[!] Invallid item price";
    }
};

struct BadCountException : public std::exception {
    const char* what() const throw () {
        return "[!] Invalid count number";
    }
};

struct CustomerExistsException : public std::exception {
    const char* what() const throw () {
        return "[!] Customer already exists";
    }
};
