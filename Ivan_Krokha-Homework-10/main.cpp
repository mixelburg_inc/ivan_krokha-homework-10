#include "Market.h"
#include <iostream>

struct MainMenu
{
	static const int exit_choice = 0;
	static const int sign_in = 1;
	static const int update = 2;
	static const int print_best = 3;

	static void print() {
		std::cout << std::endl
			<< "Welcome to MagshiMart!" << std::endl
			<< "Main menu: " << std::endl
			<< "1. sign as customer and buy items" << std::endl
			<< "2. uptade existing customer's items" << std::endl
			<< "3. print the customer who pays the most" << std::endl 
			<< "0. exit" << std::endl
			<< std::endl;
	}
	static void execute(int choice, Market* market) {
		switch (choice)
		{
		case 0:
		
			break;
		case 1:
			addCustomer(market);
			break;
		case 2:
			updateCustomer(market);
			break;
		case 3: 
			market->printBestCustomer();
			break;
		default:
			std::cout << "[!] Invalid choice" << std::endl;
			break;
		}
	}
	static void addCustomer(Market* market) {
		std::string name = MyUtil::getString(MyUtil::MSG[MyUtil::Msgs::enter_name]);
		market->addCustomer(name);
		market->buyItems(name);
	}
	static void updateCustomer(Market* market) {
		std::string name = MyUtil::getString(MyUtil::MSG[MyUtil::Msgs::enter_name]);
		try
		{
			market->updateCustomer(name);
		}
		catch (const CustomerExistsException&)
		{
			std::cout << "[!] Customer doesn't exist" << std::endl;
		}		
	}
};


int main()
{
	auto* mainMarket = new Market();

	int choice;
	do {
		MainMenu::print();
		choice = MyUtil::getNum(MyUtil::MSG[MyUtil::Msgs::enter_choice]);
		MainMenu::execute(choice, mainMarket);
	} while (choice != MainMenu::exit_choice);

	delete mainMarket;
	return 0;
}