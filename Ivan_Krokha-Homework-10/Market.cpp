#include "Market.h"

Market::Market()
{
}

Market::~Market()
{
}

void Market::addCustomer(std::string& name)
{
	if (checkCustomer(name)) throw CustomerExistsException();
	else this->customers.insert(std::pair<std::string, Customer>(name, Customer(name)));
}

void Market::updateCustomer(std::string& name)
{
	if (!(checkCustomer(name))) throw CustomerExistsException();

	int choice;
	while (true)
	{
		printUpdateMenu();
		choice = MyUtil::getNum(MyUtil::MSG[MyUtil::Msgs::enter_choice]);
		if (choice == 0) return;
		switch (choice)
		{
		case 1:
			buyItems(name);
			break;
		case 2:
			removeItems(name);
			break;
		default:
			std::cout << MyUtil::MSG[MyUtil::Msgs::invalid_choice_error] << std::endl;
			break;
		}
	}
}

void Market::buyItems(std::string& name)
{
	int choice;
	while (true)
	{
		printItems();
		choice = MyUtil::getNum(MyUtil::MSG[MyUtil::Msgs::enter_choice], 0, numItems);
		if (choice == 0) return;
		this->customers[name].addItem(this->itemList[choice - 1]);
	}
}

void Market::removeItems(std::string& name)
{
	int choice;
	while (true)
	{
		this->customers[name].printItems();
		std::cout << "0. exit" << std::endl;
		choice = MyUtil::getNum(MyUtil::MSG[MyUtil::Msgs::enter_choice]);
		if (choice == 0) return;
		this->customers[name].removeItem(this->itemList[choice - 1]);
	}
}

void Market::printItems()
{
	std::cout << "All items: " << std::endl;
	for (int i = 0; i < numItems; i++)
	{
		std::cout << i + 1 << ". " << itemList[i] << std::endl;
	}
	std::cout << "0. exit" << std::endl;
}

bool Market::checkCustomer(std::string& name)
{
	return this->customers.find(name) != this->customers.end();
}

void Market::printUpdateMenu()
{
	std::cout << std::endl
		<< "Update menu: " << std::endl
		<< "1. add items" << std::endl
		<< "2. remove items" << std::endl
		<< "0. exit"
		<< std::endl;
}

void Market::printBestCustomer()
{
	std::string bestName;
	double maxSpent = -999;
	for (std::pair<std::string, Customer> elem : this->customers) {
		if (elem.second.totalSum() > maxSpent) {
			maxSpent = elem.second.totalSum();
			bestName = elem.first;
		}
	}
	try
	{
		std::cout << "Best customer: " << bestName << std::endl;
		std::cout << "Total spent: " << this->customers[bestName].totalSum() << std::endl << std::endl;
	}
	catch (const std::exception&)
	{

	}
}

